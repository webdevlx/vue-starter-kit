import { createI18n } from "vue-i18n";
import uz from "./locales/uz.json";
import en from "./locales/en.json";
import ru from "./locales/ru.json";

const localStorageLang = localStorage.getItem("lang");

const i18n = createI18n({
  locale: localStorageLang || "en",
  messages: {
    uz,
    en,
    ru,
  },
});

export default i18n;
