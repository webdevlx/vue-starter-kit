import { useUserStore } from "../stores/user";
import axios from "axios";

axios.interceptors.request.use((request) => {
  const userStore = useUserStore();
  request.baseURL = import.meta.env.VITE_API_URL;
  if (userStore.accessToken !== "")
    request.headers.Authorization = userStore.accessToken;

  return request;
});
