import axios from "axios";
import { defineStore } from "pinia";

interface UserPostObject {
  email: string;
  password: string;
}

interface State {
  accessToken: string;
  isChecked: boolean;
  role: string;
}

export const useUserStore = defineStore("UserStore", {
  state: (): State => ({
    accessToken: localStorage.getItem("accessToken") || "",
    isChecked: false,
    role: "",
  }),
  actions: {
    auth(user: UserPostObject) {
      return new Promise((resolve, reject) => {
        axios
          .post("/users/auth", user)
          .then((response) => {
            this.accessToken = response.data.accessToken;
            localStorage.setItem("accessToken", this.accessToken);
            this.check();
            resolve(1);
          })
          .catch(() => {
            reject(0);
          });
      });
    },
    check() {
      return new Promise((resolve, reject) => {
        axios
          .post("/users/about_me", {})
          .then((response) => {
            response.data.roles[0] === "ROLE_ADMIN"
              ? (this.role = "admin")
              : (this.role = "user");
            this.isChecked = true;
            resolve(1);
          })
          .catch(() => {
            reject(0);
          });
      });
    },
  },
});
