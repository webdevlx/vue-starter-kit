import { createRouter, createWebHistory } from "vue-router";
import Index from "../views/main/Index.vue";
import Home from "../views/main/Home.vue";
import Products from "../views/main/Products.vue";
import Debts from "../views/main/Debts.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Index",
      component: Index,
      children: [
        {
          path: "",
          name: "Home",
          component: Home,
        },
        {
          path: "products",
          name: "Products",
          component: Products,
        },
        {
          path: "debts",
          name: "Debts",
          component: Debts,
        },
      ],
    },
    {
      path: "/history",
      name: "History",
      component: () => import("../views/History.vue"),
    },
    {
      path: "/:pathMatch(.*)*",
      name: "NotFound",
      component: () => import("../views/NotFound.vue"),
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { top: 0 };
    }
  },
});

export default router;
