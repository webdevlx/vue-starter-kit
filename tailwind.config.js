/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        primary: "#50429B",
        "primary-light": "#F7F5FF",
        "primary-dark": "#2B2162",
        secondary: "#FF8C21",
      },
    },
  },
  plugins: [],
};
